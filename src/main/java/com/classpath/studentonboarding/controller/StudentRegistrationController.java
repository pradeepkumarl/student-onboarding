package com.classpath.studentonboarding.controller;

import com.classpath.studentonboarding.model.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/registration")
@Slf4j
public class StudentRegistrationController {

    @PostMapping
    public Student onboardStudent(@RequestBody Student student){
      log.info("Request came inside the orboardStudent method ::::");
      log.info("=======Onboarding student "+ student.getName()+ "============");
      return student;
    }
}